(function () {
    angular
        .module('chatApp')
        .factory('chatService', chatService);

//    chatService.$inject = [];

    function chatService() {
        var service = this;
        var messageArray = [];

        // Exposed functions/services
        return service = {
            getData: getData,
            setData: setData
        };

        // Function declaration and definition
        function getData() {

            return messageArray;
        }

        function setData(data) {

            messageArray.push(data);
        }
    }

})();
