(function () {
    angular
        .module("chatApp")
        .controller("ChatController", ChatController);

    ChatController.$inject = ['chatService'];

    // ChatCtrl controls the middle window where Alex's and Steve's chat messages appear
    function ChatController(chatService) {
        var vm = this;

        vm.messageArray = chatService.getData();
    }

})();