(function () {
    angular
        .module("chatApp")
        .controller("ChatterOneCtrl", ChatterOneCtrl);

    ChatterOneCtrl.$inject = ['chatService'];

    // ChatterOneCtrl controls the left chat window (i.e., where Alex types his messages)
    function ChatterOneCtrl(chatService) {
        var vm = this;
        var messagePayload = {};

        // Initializations
        vm.message = '';    // this data is exposed to the view (HTML)

        // Exposed functions
        vm.sendMessage = sendMessage;

        // Function declaration and definition
        function sendMessage(){
            var messagePayload = {};

            messagePayload.chatterName = "Alex"; // hardcoded chatter. it should be easy enough to make this dynamic.
            messagePayload.message = vm.message;

            chatService
                .setData(messagePayload);

            vm.message = '';
        }
    }

})();