(function () {
    angular
        .module("chatApp")
        .controller("ChatterTwoCtrl", ChatterTwoCtrl);

    ChatterTwoCtrl.$inject = ['chatService'];

    // ChatterTwoCtrl controls the right chat window (i.e., where Steve types his messages)
    function ChatterTwoCtrl(chatService) {
        var vm = this;
        var messagePayload = {};

        // Initializations
        vm.message = '';    // this data is exposed to the view (HTML)

        // Exposed functions
        vm.sendMessage = sendMessage;

        // Function declaration and definition
        function sendMessage(){
            var messagePayload = {};

            messagePayload.chatterName = "Steve"; // hardcoded chatter. it should be easy enough to make this dynamic.
            messagePayload.message = vm.message;

            chatService
                .setData(messagePayload);

            vm.message = '';
        }
    }

})();